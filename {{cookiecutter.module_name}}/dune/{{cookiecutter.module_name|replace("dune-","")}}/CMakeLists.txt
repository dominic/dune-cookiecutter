install(FILES {{ cookiecutter.module_name | replace("dune-", "") }}.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/{{ cookiecutter.module_name | replace("dune-", "") }}
        )
