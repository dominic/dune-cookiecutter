# What is dune-cookiecutter?

dune-cookiecutter is a [cookiecutter template](https://github.com/cookiecutter/cookiecutter)
for a [Dune module](www.dune-project.org).

# Requirements

dune-cookiecutter requires cookiecutter, which is a Python package that can be
installed through your favorite method of installing Python packages. I recommend:

```
python -m pip install cookiecutter
```

# How to use dune-cookiecutter

Cookiecutter is a stand-alone executable. You can pass either a path to this template
or a URL to a git repository with this template:

```
cookiecutter <path/to/dune-cookiecutter>
cookiecutter https://gitlab.dune-project.org/dominic/dune-cookiecutter
```

The command will prompt you for the required information to set up a fully functional Dune module.

# Questions, Feedback?

Please consider opening an issue on this project.
