# This hook is executed after the project template is successfully
# configured. It can e.g. be used to remove files that should only
# be included if a certain configuration value is given.

import os

# Rename dune.module.template -> dune.module
# The reason for this is that if a clone of the template repository
# dune-cookiecutter is found in DUNE_CONTROL_PATH, dunecontrol falls
# over because it does not accept the Jinja template name. This
# circumvents the problem, another solution could be to make dunecontrol
# ignore more dune.module files that it does not understand.
os.rename("dune.module.template" , "dune.module")
